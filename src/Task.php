<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 02.05.2018
 * Time: 1:58
 */

namespace aRoseen\Redis;

/**
 * Class Task
 * @package aRoseen\Redis
 */
abstract class Task
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->identifier = $this->makeIdentifier();
    }

    /**
     * @return void
     */
    abstract public function process(): void;

    /**
     * @return string
     */
    protected function makeIdentifier(): string
    {
        return str_shuffle(str_replace('.', null, uniqid(md5(\get_class($this)), true)));
    }
}