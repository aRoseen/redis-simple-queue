<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 02.05.2018
 * Time: 1:54
 */

namespace aRoseen\Redis\Queues;

use aRoseen\Redis\Task;

/**
 * Class DelayedQueue
 * @package aRoseen\Redis\Queues
 */
final class DelayedQueue extends RedisQueue
{
    public const PREFIX = 'delayed:';

    /**
     * @param Task   $value
     * @param int    $delay
     * @param string $priority
     * @return mixed
     */
    public function push(Task $value, int $delay, string $priority = self::PRIORITY_LOW)
    {
        $score = time() + $delay;

        return $this->redisClient->zadd($this->channelName($priority), [serialize($value) => $score]);
    }

    /**
     * @param callable $lockCallback
     * @param string   $priority
     * @return string
     */
    public function pull(callable $lockCallback, ?string $priority = null): ? string
    {
        $allPriorityQueues = $priority ? [$this->channelName($priority)] : $this->allPriorityQueues();
        foreach ($allPriorityQueues as $priorityQueue) {
            $pickedItem = $this->redisClient->zrange($priorityQueue, 0, 0, 'withscores');
            if ($pickedItem && (current($pickedItem) <= time())) {
                $serializedTask = key($pickedItem);
                $lock           = md5($serializedTask);
                if ($this->acquireLock($lock)) {
                    $result = $lockCallback($this->redisClient, $serializedTask, $priorityQueue, $priority);
                    $this->releaseLock($lock);

                    return $result ? $serializedTask : null;
                }
            }
            continue;
        }

        return null;
    }

    /**
     * @param string $priority
     * @return int|null
     */
    public function length(string $priority = self::PRIORITY_LOW): ?int
    {
        return $this->redisClient->zcard($this->channelName($priority));
    }
}