<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 02.05.2018
 * Time: 0:05
 */

namespace aRoseen\Redis\Queues;

use aRoseen\Redis\Clients\PredisClient;
use aRoseen\Redis\Clients\RedisClient;
use Predis\Client as Predis;
use RuntimeException;

/**
 * Class RedisQueue
 * @package aRoseen\Redis
 */
abstract class RedisQueue
{
    public const PREFIX      = 'queue:';
    public const LOCK_PREFIX = 'lock:';

    public const PRIORITY_HIGH   = 'high';
    public const PRIORITY_MEDIUM = 'medium';
    public const PRIORITY_LOW    = 'low';

    /**
     * @var RedisClient|Predis
     */
    protected $redisClient;

    /**
     * RedisQueue constructor.
     * @param RedisClient|array $credentialsOrClient
     */
    public function __construct($credentialsOrClient)
    {
        if ($credentialsOrClient instanceof RedisClient) {
            $this->redisClient = $credentialsOrClient;
        } elseif (\is_array($credentialsOrClient) && \count($credentialsOrClient) !== 0) {
            $this->redisClient = new PredisClient($credentialsOrClient);
        } else {
            throw new RuntimeException('Redis client is not defined or empty credentials.');
        }
    }

    /**
     * @return RedisClient
     */
    public function getRedisClient(): RedisClient
    {
        return $this->redisClient;
    }

    /**
     * @param RedisClient $redisClient
     * @return RedisQueue
     */
    public function setRedisClient(RedisClient $redisClient): self
    {
        $this->redisClient = $redisClient;

        return $this;
    }

    /**
     * @param string $lockname
     * @param int    $lockTimeout
     * @return bool
     */
    protected function acquireLock(string $lockname, int $lockTimeout = 10): bool
    {
        $lockname = static::LOCK_PREFIX.$lockname;
        if ($this->redisClient->setnx($lockname, 1)) {
            $this->redisClient->expire($lockname, $lockTimeout);

            return true;
        }

        return false;
    }

    /**
     * @param string $lockname
     * @return bool
     */
    protected function releaseLock(string $lockname): bool
    {
        $lockname = static::LOCK_PREFIX.$lockname;

        return (bool) $this->redisClient->del([$lockname]);
    }

    /**
     * @param string|null $priority
     * @return string
     */
    protected function channelName(string $priority = null): string
    {
        return static::PREFIX.$priority;
    }

    /**
     * @return array
     */
    protected function allPriorityQueues(): array
    {
        return [$this->channelName(self::PRIORITY_HIGH), $this->channelName(self::PRIORITY_MEDIUM), $this->channelName(self::PRIORITY_LOW)];
    }
}