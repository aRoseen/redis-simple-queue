<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 02.05.2018
 * Time: 1:53
 */

namespace aRoseen\Redis\Queues;

use aRoseen\Redis\Task;

/**
 * Class Queue
 * @package aRoseen\Redis\Queues
 */
final class PriorityQueue extends RedisQueue
{
    /**
     * @param Task   $value
     * @param string $priority
     * @return mixed
     */
    public function push(Task $value, string $priority = self::PRIORITY_LOW)
    {
        return $this->redisClient->lpush($this->channelName($priority), [serialize($value)]);
    }

    /**
     * @param string $priority
     * @param bool   $isBlocking
     * @return mixed
     */
    public function pull(bool $isBlocking = true, string $priority = self::PRIORITY_LOW)
    {
        return $isBlocking ? $this->redisClient->brpop($this->allPriorityQueues(), 0) : $this->redisClient->rpop($this->channelName($priority));
    }

    /**
     * @param string $priority
     * @return int|null
     */
    public function length(string $priority = self::PRIORITY_LOW): ?int
    {
        return $this->redisClient->llen($this->channelName($priority));
    }
}