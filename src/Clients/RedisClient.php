<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 01.05.2018
 * Time: 22:27
 */

namespace aRoseen\Redis\Clients;

use RuntimeException;

/**
 * Interface RedisClient
 *
 * @package aRoseen\Redis\Clients
 */
abstract class RedisClient
{
    /**
     * @var mixed
     */
    protected $client;

    /**
     * RedisClient constructor.
     * @param array $credentials
     * @throws RuntimeException
     */
    public function __construct(array $credentials)
    {
        if (empty($credentials['host']) || empty($credentials['port'])) {
            throw new RuntimeException('Host or port is empty.');
        }
        $this->client = $this->initClient($credentials);
    }

    /**
     * @return mixed
     */
    public function getClient(): mixed
    {
        return $this->client;
    }

    /**
     * @param array $credentials
     * @return mixed
     */
    abstract protected function initClient(array $credentials);

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->client->{$name}(...$arguments);
    }

}