<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 02.05.2018
 * Time: 1:06
 */

namespace aRoseen\Redis\Clients;

use Predis\Client as Predis;

/**
 * Class PredisClient
 *
 *
 * @property Predis $client
 *
 *
 * @package aRoseen\Redis\Clients
 */
class PredisClient extends RedisClient
{
    /**
     * @param array $credentials
     * @return mixed
     */
    protected function initClient(array $credentials)
    {
        return new Predis([
            'scheme' => 'tcp',
            'host'   => $credentials['host'],
            'port'   => $credentials['port'],
        ]);
    }
}