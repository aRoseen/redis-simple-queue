<?php
/**
 * Created by PhpStorm.
 * User: aRosen_LN
 * Date: 09.05.2018
 * Time: 16:05
 */

namespace aRoseen\Redis;

use aRoseen\Redis\Clients\RedisClient;
use aRoseen\Redis\Queues\DelayedQueue;
use aRoseen\Redis\Queues\PriorityQueue;
use aRoseen\Redis\Queues\RedisQueue;
use Predis\Client as Predis;
use Generator;
use RuntimeException;

/**
 * Class QueueManager
 * @package aRoseen\Redis
 */
final class QueueManager
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @var PriorityQueue
     */
    private $priorityQueue;

    /**
     * @var DelayedQueue
     */
    private $delayedQueue;

    /**
     * QueueManager constructor.
     */
    private function __construct()
    {

    }

    /**
     * @return void
     */
    public function __clone()
    {
        throw new RuntimeException('Cloning is not allowed.');
    }

    /**
     * @return array
     */
    public function __sleep(): array
    {
        throw new RuntimeException('Serializing is not allowed.');
    }

    /**
     * @param PriorityQueue|null $priorityQueue
     * @param DelayedQueue|null  $delayedQueue
     * @return QueueManager
     */
    public static function instance(?PriorityQueue $priorityQueue = null, ?DelayedQueue $delayedQueue = null): self
    {
        if (!self::$instance) {
            self::$instance = new self();

            if ($priorityQueue) {
                self::$instance->setPriorityQueue($priorityQueue);
            }

            if ($delayedQueue) {
                self::$instance->setDelayedQueue($delayedQueue);
            }
        }

        return self::$instance;
    }

    /**
     * @param PriorityQueue $queue
     * @return QueueManager
     */
    public function setPriorityQueue(PriorityQueue $queue): self
    {
        $this->priorityQueue = $queue;

        return $this;
    }

    /**
     * @param DelayedQueue $queue
     * @return QueueManager
     */
    public function setDelayedQueue(DelayedQueue $queue): self
    {
        $this->delayedQueue = $queue;

        return $this;
    }

    /**
     * @param Task   $task
     * @param string $priority
     */
    public function enqueueTask(Task $task, string $priority = RedisQueue::PRIORITY_LOW): void
    {
        $this->priorityQueue->push($task, $priority);
    }

    /**
     * @param Task     $task
     * @param string   $priority
     * @param int|null $delay
     */
    public function enqueueDelayedTask(Task $task, int $delay, string $priority = RedisQueue::PRIORITY_LOW): void
    {
        if ($delay === 0) {
            $this->priorityQueue->push($task, $priority);
        } else {
            $this->delayedQueue->push($task, $delay, $priority);
        }
    }

    /**
     * @param null|string $priority
     * @return Generator
     */
    public function dequeueTasks(?string $priority = null): Generator
    {
        while (true) {
            if ($item = $priority ? $this->priorityQueue->pull(false, $priority) : $this->priorityQueue->pull()) {
                yield unserialize(\is_array($item) ? $item[1] : $item, ['allowed_classes' => true]);
            }
            continue;
        }
    }

    /**
     * @param null|string $priority
     * @return Generator
     */
    public function dequeueDelayedTasks(?string $priority = null): Generator
    {
        while (true) {
            $dequeue = $this->delayedQueue->pull(function (RedisClient $redisClient, string $serializedTask, string $queueName, $priority) {
                /** @var RedisClient|Predis $redisClient */
                if (!$redisClient->zrem($queueName, $serializedTask)) {
                    return false;
                }
                $item = unserialize($serializedTask, ['allowed_classes' => true]);
                $priority ? $this->priorityQueue->push($item, $priority) : $this->priorityQueue->push($item);

                return true;
            }, $priority);
            if ($dequeue) {
                yield unserialize($dequeue, ['allowed_classes' => true]);
            }
            continue;
        }
    }
}